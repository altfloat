{-
 - Copyright (C) 2010 Nick Bowler.
 -
 - License BSD2:  2-clause BSD license.  See LICENSE for full terms.
 - This is free software: you are free to change and redistribute it.
 - There is NO WARRANTY, to the extent permitted by law.
 -}

-- | Demonstration of why the functions in "Data.Floating.Environment" are
-- not safe for use concurrently with unbound threads that perform floating
-- point operations.

{-# LANGUAGE NoImplicitPrelude #-}
module Main where

import Data.Floating.Prelude
import Data.Floating.Environment
import Control.Concurrent
import Control.Exception

theThread :: MVar () -> MVar Double -> IO ()
theThread input output = do
    takeMVar input
    evaluate (1/10) >>= putMVar output

main :: IO ()
main = do
    input  <- newEmptyMVar
    output <- newEmptyMVar
    forkIO $ theThread input output
    ref <- evaluate (1 / 10)
    -- This rounding mode should not be visible to the other thread.
    unsafeSetRoundingMode TowardZero
    putMVar input ()
    ret <- takeMVar output
    -- At this point, the output should equal the reference value.
    print $ ref == ret
