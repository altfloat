/*
 * Simple implementations of some C99 library functions.
 * Note that the goal of altfloat is not to create a C99 math library: these
 * functions are intended to just be "good enough" on platforms where they are
 * missing.  They likely do not fully conform to applicable standards.
 *
 * Copyright (C) 2010 Nick Bowler.
 *
 * License BSD2:  2-clause BSD license.  See LICENSE for full terms.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <config.h>
#include <limits.h>
#include <math.h>

#if NEED_LIBM_NAN
double nan(const char *tagp)
{
	return NAN;
}
#endif

#if NEED_LIBM_LOG2
double log2(double x)
{
	return log(x) / log(2);
}
#endif

#if NEED_LIBM_EXP2
double exp2(double x)
{
	return pow(2, x);
}
#endif

#if NEED_LIBM_FMA
double fma(double x, double y, double z)
{
	return (long double) x*y + z;
}
#endif

#if NEED_LIBM_REMQUO
double remquo(double x, double y, int *quo)
{
	unsigned tmp = fabs(nearbyint(x/y));
	int sign     = signbit(x/y) ? -1 : 1;

	*quo = sign * (int)(tmp % (INT_MAX + 1u));
	return remainder(x, y);
}
#endif

/*
 * Float-valued functions implemented in terms of double-valued functions.
 */
#if NEED_LIBM_ACOSF
float acosf(float x)
{
	return acos(x);
}
#endif

#if NEED_LIBM_ASINF
float asinf(float x)
{
	return asin(x);
}
#endif

#if NEED_LIBM_ATANF
float atanf(float x)
{
	return atan(x);
}
#endif

#if NEED_LIBM_ATAN2F
float atan2f(float x, float y)
{
	return atan2(x, y);
}
#endif

#if NEED_LIBM_COSF
float cosf(float x)
{
	return cos(x);
}
#endif

#if NEED_LIBM_SINF
float sinf(float x)
{
	return sin(x);
}
#endif

#if NEED_LIBM_TANF
float tanf(float x)
{
	return tan(x);
}
#endif

#if NEED_LIBM_ACOSHF
float acoshf(float x)
{
	return acosh(x);
}
#endif

#if NEED_LIBM_ASINHF
float asinhf(float x)
{
	return asinh(x);
}
#endif

#if NEED_LIBM_ATANHF
float atanhf(float x)
{
	return atanh(x);
}
#endif

#if NEED_LIBM_COSHF
float coshf(float x)
{
	return cosh(x);
}
#endif

#if NEED_LIBM_SINHF
float sinhf(float x)
{
	return sinh(x);
}
#endif

#if NEED_LIBM_TANHF
float tanhf(float x)
{
	return tanh(x);
}
#endif

#if NEED_LIBM_EXPF
float expf(float x)
{
	return exp(x);
}
#endif

#if NEED_LIBM_EXP2F
float exp2f(float x)
{
	return exp2(x);
}
#endif

#if NEED_LIBM_EXPM1F
float expm1f(float x)
{
	return expm1(x);
}
#endif

#if NEED_LIBM_FREXPF
float frexpf(float x, int *exp)
{
	return frexp(x, exp);
}
#endif

#if NEED_LIBM_ILOGBF
int ilogbf(float x)
{
	return ilogb(x);
}
#endif

#if NEED_LIBM_LDEXPF
float ldexpf(float x, int exp)
{
	return ldexp(x, exp);
}
#endif

#if NEED_LIBM_LOGF
float logf(float x)
{
	return log(x);
}
#endif

#if NEED_LIBM_LOG10F
float log10f(float x)
{
	return log10(x);
}
#endif

#if NEED_LIBM_LOG1PF
float log1pf(float x)
{
	return log1p(x);
}
#endif

#if NEED_LIBM_LOG2F
float log2f(float x)
{
	return log2(x);
}
#endif

#if NEED_LIBM_LOGBF
float logbf(float x)
{
	return logb(x);
}
#endif

#if NEED_LIBM_MODFF
float modff(float x, float *iptr)
{
	double tmp, ret;

	ret = modf(x, &tmp);
	*iptr = tmp;
	return ret;
}
#endif

#if NEED_LIBM_SCALBNF
float scalbnf(float x, int exp)
{
	return scalbn(x, exp);
}
#endif

#if NEED_LIBM_SCALBLNF
float scalblnf(float x, long exp)
{
	return scalbln(x, exp);
}
#endif

#if NEED_LIBM_CBRTF
float cbrtf(float x)
{
	return cbrt(x);
}
#endif

#if NEED_LIBM_FABSF
float fabsf(float x)
{
	return fabs(x);
}
#endif

#if NEED_LIBM_HYPOTF
float hypotf(float x, float y)
{
	return hypot(x, y);
}
#endif

#if NEED_LIBM_POWF
float powf(float x, float y)
{
	return pow(x, y);
}
#endif

#if NEED_LIBM_SQRTF
float sqrtf(float x)
{
	return sqrt(x);
}
#endif

#if NEED_LIBM_FMODF
float fmodf(float x, float y)
{
	return fmod(x, y);
}
#endif

#if NEED_LIBM_REMAINDERF
float remainderf(float x, float y)
{
	return remainder(x, y);
}
#endif

#if NEED_LIBM_REMQUOF
float remquof(float x, float y, int *quo)
{
	return remquo(x, y, quo);
}
#endif

#if NEED_LIBM_COPYSIGNF
float copysignf(float x, float y)
{
	return copysign(x, y);
}
#endif

#if NEED_LIBM_NANF
float nanf(const char *tagp)
{
	return nan(tagp);
}
#endif

#if NEED_LIBM_ERFF
float erff(float x)
{
	return erf(x);
}
#endif

#if NEED_LIBM_ERFCF
float erfcf(float x)
{
	return erfc(x);
}
#endif

#if NEED_LIBM_LGAMMAF
float lgammaf(float x)
{
	return lgamma(x);
}
#endif

#if NEED_LIBM_TGAMMAF
float tgammaf(float x)
{
	return tgamma(x);
}
#endif

#if NEED_LIBM_CEILF
float ceilf(float x)
{
	return ceil(x);
}
#endif

#if NEED_LIBM_FLOORF
float floorf(float x)
{
	return floor(x);
}
#endif

#if NEED_LIBM_NEARBYINTF
float nearbyintf(float x)
{
	return nearbyint(x);
}
#endif

#if NEED_LIBM_RINTF
float rintf(float x)
{
	return rint(x);
}
#endif

#if NEED_LIBM_LRINTF
long lrintf(float x)
{
	return lrint(x);
}
#endif

#if NEED_LIBM_LLRINTF
long long llrintf(float x)
{
	return llrint(x);
}
#endif

#if NEED_LIBM_ROUNDF
float roundf(float x)
{
	return round(x);
}
#endif

#if NEED_LIBM_LROUNDF
long lroundf(float x)
{
	return lround(x);
}
#endif

#if NEED_LIBM_LLRINTF
long long llroundf(float x)
{
	return llround(x);
}
#endif

#if NEED_LIBM_TRUNCF
float truncf(float x)
{
	return trunc(x);
}
#endif

#if NEED_LIBM_FDIMF
float fdimf(float x, float y)
{
	return fdim(x, y);
}
#endif

#if NEED_LIBM_FMAXF
float fmaxf(float x, float y)
{
	return fmax(x, y);
}
#endif

#if NEED_LIBM_FMINF
float fminf(float x, float y)
{
	return fmin(x, y);
}
#endif

#if NEED_LIBM_FMAF
float fmaf(float x, float y, float z)
{
	return fma(x, y, z);
}
#endif
