{-
 - Copyright (C) 2009-2010 Nick Bowler.
 -
 - License BSD2:  2-clause BSD license.  See LICENSE for full terms.
 - This is free software: you are free to change and redistribute it.
 - There is NO WARRANTY, to the extent permitted by law.
 -}

-- | A replacement for the 'RealFrac' class which is usable by floating point
-- types.  The functions in 'RealFrac' shoehorn their results into an integer,
-- which means they simply cannot be defined sensibly for infinities or NaNs.
module Data.Roundable where

import Prelude hiding (Ord(..), ceiling, floor, truncate, round)
import Data.Ratio
import Data.Poset

-- | Class for ordered numeric types which embed a subset of the integers.
--
-- Minimal complete definition: 'toIntegral' and 'round'.
class (Fractional a, Poset a) => Roundable a where
    -- | Discards the fractional component from a value.  Results in 'Nothing'
    -- if the result cannot be represented as an integer, such as if the input
    -- is infinite or NaN.
    toIntegral :: Integral b => a -> Maybe b
    -- | Determine the least integer not less than the input.
    ceiling    :: a -> a
    -- | Determine the greatest integer not greater than the input.
    floor      :: a -> a
    -- | Determine the greatest integer whose absolute value is not greater
    -- than the input.
    truncate   :: a -> a
    -- | Determine the integer closest to the input.  In case of a tie, the
    -- integer largest in absolute value is chosen.
    round      :: a -> a

    floor x
        | round x == x = x
        | otherwise = round $ x - fromRational (1%2)
    ceiling x
        | round x == x = x
        | otherwise = round $ x + fromRational (1%2)
    truncate x
        | x < 0 = ceiling x
        | x > 0 = floor x
        | otherwise = x

instance Integral a => Roundable (Ratio a) where
    toIntegral = Just . fst . properFraction
    round x
        | abs frac >= 1%2 = int%1 + signum frac
        | otherwise       = int%1
        where (int, frac) = properFraction x
