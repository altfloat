{-
 - Copyright (C) 2009-2010 Nick Bowler.
 -
 - License BSD2:  2-clause BSD license.  See LICENSE for full terms.
 - This is free software: you are free to change and redistribute it.
 - There is NO WARRANTY, to the extent permitted by law.
 -}

-- | Core floating point types and classes.
{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, OverlappingInstances #-}
{-# LANGUAGE MagicHash #-}
module Data.Floating.Types.Core where

import Prelude hiding
    (Double, Float, Ord(..), RealFrac(..), Floating(..), RealFloat(..))

import Data.Ratio
import Data.Roundable
import Data.Poset

import GHC.Integer
import GHC.Prim
import Foreign.C
import Unsafe.Coerce

-- | The Double type.  This is expected to be an identical declaration to
-- the one found in "GHC.Types".  We redefine it in order to replace all its
-- instances.
data Double = D# Double#

-- | The Float type.
data Float  = F# Float#

-- | Classification of floating point values.
data FPClassification = FPInfinite | FPNaN | FPNormal | FPSubNormal | FPZero
    deriving (Show, Read, Eq, Enum, Bounded)

-- | Class for floating point types (real or complex-valued).
--
-- Minimal complete definition: everything.
class Fractional a => Floating a where
    (**)  :: a -> a -> a
    sqrt  :: a -> a
    acos  :: a -> a
    asin  :: a -> a
    atan  :: a -> a
    cos   :: a -> a
    sin   :: a -> a
    tan   :: a -> a
    acosh :: a -> a
    asinh :: a -> a
    atanh :: a -> a
    cosh  :: a -> a
    sinh  :: a -> a
    tanh  :: a -> a
    exp   :: a -> a
    log   :: a -> a

-- | Class for real-valued floating point types.
--
-- Minimal complete definition: all except 'pi', 'infinity', 'epsilon' and
-- 'nan'.
class Floating a => RealFloat a where
    -- | Fused multiply-add.
    fma       :: a -> a -> a -> a
    -- | @copysign x y@ computes a value with the magnitude of @x@ but the sign
    -- of @y@.
    copysign  :: a -> a -> a
    -- | @nextafter x y@ computes the next representable value after @x@ in the
    -- direction of @y@.
    nextafter :: a -> a -> a
    -- | @atan2 y x@ computes the principal value of the arctangent of @y/x@.
    -- The signs of the input determine the quadrant of the result.
    atan2     :: a -> a -> a
    -- | @fmod x y@ computes @x - n*y@, where @n@ is the integral quotient of
    -- @x/y@, rounded towards zero.
    fmod      :: a -> a -> a
    -- | @frem x y@ computes @x - n*y@, where @n@ is the integral quotient of
    -- @x/y@, rounded to the nearest integer, with halfway values rounded to
    -- even.
    frem      :: a -> a -> a
    -- | Euclidean distance function without undue overflow.
    hypot     :: a -> a -> a
    -- | Cube root.
    cbrt      :: a -> a
    -- | Base-2 exponential function.
    exp2      :: a -> a
    -- | Computes @exp x - 1@ without undue cancellation.
    expm1     :: a -> a
    -- | Base-10 logarithm function.
    log10     :: a -> a
    -- | Computes @log (x + 1)@ without undue cancellation.
    log1p     :: a -> a
    -- | Base-2 logarithm function.
    log2      :: a -> a
    -- | Error function.
    erf       :: a -> a
    -- | Complementary error function.
    erfc      :: a -> a
    -- | Gamma function.
    gamma    :: a -> a
    -- | Log gamma function.
    lgamma    :: a -> a
    -- | Round to the nearest integer according to the current rounding
    -- direction.  The default rounding direction is towards the nearest
    -- integer with halfway values rounded to even.  If the resulting value
    -- differs from the input, the 'Inexact' exception is raised.
    rint      :: a -> a
    -- | Same as 'rint', except that the 'Inexact' exception is not raised.
    nearbyint :: a -> a
    infinity  :: a
    nan       :: a
    epsilon   :: a
    pi        :: a

    infinity = 1/0
    nan      = 0/0
    epsilon  = nextafter 1 infinity - 1
    pi       = 4 * atan 1

-- | Class for the basic floating point types.
class (Roundable a, Sortable a, RealFloat a) => PrimFloat a where
    -- | Radix of significand digits.
    floatRadix     :: Num b => a -> b
    -- | Number of digits in the significand.
    floatPrecision :: Num b => a -> b
    -- | Minimum and maximum integers, respectively, such that the radix raised
    -- to one less than that power is representable as a normalized, finite
    -- floating point number.
    floatRange     :: Num b => a -> (b, b)
    classify       :: a -> FPClassification
    -- | Extracts the exponent of a floating point value.  If the value is
    -- subnormal, the result is as if the value were normalized.
    logb           :: a -> a
    -- | Scales a floating point value by an integral power of the radix.
    scalb          :: Integral b => a -> b -> a

infix 6 :+
-- | Complex numbers.
data (PrimFloat a) => Complex a = !a :+ !a
    deriving Eq

-- | Coercion to floating point types.
class FloatConvert a b where
    -- | Convert to a floating point type.  Conversions from integers and real
    -- types are provided, as well as conversions between floating point types.
    -- Conversions between floating point types preserve infinities, negative
    -- zeros and NaNs.
    toFloating :: a -> b

instance FloatConvert Double CDouble where
    toFloating = unsafeCoerce

instance FloatConvert CDouble Double where
    toFloating = unsafeCoerce

instance FloatConvert Float CFloat where
    toFloating = unsafeCoerce

instance FloatConvert CFloat Float where
    toFloating = unsafeCoerce

instance FloatConvert Double Float where
    toFloating (D# x) = F# (double2Float# x)

instance FloatConvert Float Double where
    toFloating (F# x) = D# (float2Double# x)

instance FloatConvert Integer Double where
    toFloating x = D# (doubleFromInteger x)

instance FloatConvert Integer Float where
    toFloating x = F# (floatFromInteger x)

instance Real a => FloatConvert a Double where
    toFloating x = D# (num /## denom) where
        !(D# num)   = toFloating . numerator   . toRational $ x
        !(D# denom) = toFloating . denominator . toRational $ x

instance Real a => FloatConvert a Float where
    toFloating x = F# (divideFloat# num denom) where
        !(F# num)   = toFloating . numerator   . toRational $ x
        !(F# denom) = toFloating . denominator . toRational $ x

instance FloatConvert a a where
    toFloating = id
