{-
 - Copyright (C) 2010 Nick Bowler.
 -
 - License BSD2:  2-clause BSD license.  See LICENSE for full terms.
 - This is free software: you are free to change and redistribute it.
 - There is NO WARRANTY, to the extent permitted by law.
 -}

-- | Internal helper functions needed by at least two modules.
{-# LANGUAGE ForeignFunctionInterface #-}
module Data.Floating.Helpers (
    binarySearch, scaleRational, formatDouble
) where

import Prelude hiding (Double, RealFloat(..), RealFrac(..))
import Data.Floating.Types.Core
import Data.Roundable
import Data.Maybe

import Foreign
import Foreign.C

foreign import ccall unsafe "double_format"
    double_format :: CString -> CChar -> CInt -> CDouble -> IO CInt

-- | @binarySearch p low high@ computes the least integer on the interval
-- [low, high] satisfying the given predicate, by binary search.  The
-- predicate must partition the interval into two contiguous regions.
binarySearch :: Integral a => (a -> Bool) -> a -> a -> a
binarySearch p l u
    | l >  u    = error "empty interval"
    | l == u    = m
    | p m       = binarySearch p l m
    | otherwise = binarySearch p (m+1) u
    where
        m = l + div (u-l) 2

-- | Find a power of two such that the given rational number, when multiplied
-- by that power and rounded to an integer, has exactly as many digits as the
-- precision of the floating point type.  The search may stop for values with
-- extremely large (or small) magnitude, in which case the result shall
-- overflow (or underflow) when scaled to the floating type.
scaleRational :: PrimFloat a => a -> Rational -> (Integer, Int)
scaleRational t x = (fromJust . toIntegral . round . scale x $ e, e) where
    e         = binarySearch ((lbound <=) . scale x) l (u*2)
    (l, u)    = floatRange t
    lbound    = floatRadix t ^ (floatPrecision t - 1)
    scale x y = x * 2^^y

formatDouble :: Char -> Int -> Double -> String
formatDouble c p x = unsafePerformIO $ do
    let format = castCharToCChar c
    size <- double_format nullPtr format (fromIntegral p) (toFloating x)
    allocaArray0 (fromIntegral size) $ \buf -> do
        double_format buf format (fromIntegral p) (toFloating x)
        peekCString buf
