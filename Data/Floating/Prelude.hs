{-
 - Copyright (C) 2009 Nick Bowler.
 -
 - License BSD2:  2-clause BSD license.  See LICENSE for full terms.
 - This is free software: you are free to change and redistribute it.
 - There is NO WARRANTY, to the extent permitted by law.
 -}

-- | Alternate prelude for the alternate floating point types.  This module
-- re-exports the Data.Floating and Data.Poset operations as well as all the
-- Prelude operations that do not conflict.
module Data.Floating.Prelude (
    module Data.Floating,
    module Data.Roundable,
    module Data.Poset,
    module Prelude
) where

import Prelude hiding (
    Floating(..), RealFloat(..),
    Ordering(..), Ord(..),
    Double, Float,
    round, truncate, ceiling, floor
    )

import Data.Floating
import Data.Roundable
import Data.Poset
