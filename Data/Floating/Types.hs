{-
 - Copyright (C) 2010 Nick Bowler.
 -
 - License BSD2:  2-clause BSD license.  See LICENSE for full terms.
 - This is free software: you are free to change and redistribute it.
 - There is NO WARRANTY, to the extent permitted by law.
 -}

-- | Floating point types, classes and instances.  The class interface is
-- loosely based off of the C standard library; if a function here has the
-- same name as a C function, it should behave similarly.
module Data.Floating.Types (
    module Data.Floating.Types.Core
) where

import Data.Floating.Types.Core
import Data.Floating.Types.Double ()
import Data.Floating.Types.Float  ()
import Data.Floating.Types.CMath  ()
