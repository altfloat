#ifndef CFLOAT_H_
#define CFLOAT_H_

#include <fenv.h>

int double_format(char *buf, char spec, int precision, double val);
double double_signum(double val);
float float_signum(float val);
int double_classify(double val);
int float_classify(float val);
int double_compare(double a, double b);
int float_compare(float a, float b);

int set_roundmode(int mode);
int get_roundmode(void);
int fenv_restore(fenv_t *env, unsigned *excepts);
int fenv_raise_excepts(unsigned excepts);

#endif
