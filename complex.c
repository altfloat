#include <complex.h>

#define complex_wrap1(T, func) void func ## _wrap(T complex *a) { \
	*a = func(*a); \
}

complex_wrap1(double, cacos)
complex_wrap1(double, casin)
complex_wrap1(double, catan)
complex_wrap1(double, ccos)
complex_wrap1(double, csin)
complex_wrap1(double, ctan)

complex_wrap1(double, cacosh)
complex_wrap1(double, casinh)
complex_wrap1(double, catanh)
complex_wrap1(double, ccosh)
complex_wrap1(double, csinh)
complex_wrap1(double, ctanh)

double cabs_wrap(const double complex *a)
{
	return cabs(*a);
}

complex_wrap1(double, cexp)
complex_wrap1(double, clog)

complex_wrap1(double, csqrt)

void cpow_wrap(double complex *a, const double complex *b)
{
	*a = cpow(*a, *b);
}

complex_wrap1(double, cproj)

double carg_wrap(const double complex *a)
{
	return carg(*a);
}
